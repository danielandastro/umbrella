﻿using System;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;
namespace Umbrella
{
    class reader
    {
        public static City search(string city)
        {
            string json = File.ReadAllText("city.list.json");
            var data = JsonConvert.DeserializeObject<List<City>>(json);
            foreach (var daa in data)
            {
                if (daa.name.ToLower().Equals(city.ToLower()))
                {
                    return daa;
                }
            }
            var nullcity = new City();
            nullcity.name = "not found";
            return nullcity;
        }
        
        public static List<City> multisearch(string city)
        {
            string json = File.ReadAllText("city.list.json");
            var data = JsonConvert.DeserializeObject<List<City>>(json);
            var list = new List<City>();
            foreach (var daa in data)
            {
                if (daa.name.ToLower().Contains(city.ToLower()))
                {
                    list.Add(daa);
                }
            }
            return list;
        }

        public static List<City> returnall ()
        {
            string json = File.ReadAllText("city.list.json");
            var data = JsonConvert.DeserializeObject<List<City>>(json);
            return data;
        }


        public struct City
        {
            public string id;
            public string name;
            public string country;
            public coords coord;

        }
        public struct coords
        {
            public string lat;
            public string lon;
        }
    }
}
